package Helpers;

public interface Config {

    String url = "http://www.omdbapi.com/";
    String apiParam = "apikey";
    String apiKey = "BanMePlz";
    String titleParam = "t";
    String basicMovie = "avengers";
    String contentTypeParam = "r";
    String XML = "xml";
    String plotParam = "plot";
    String plotKeyFull = "full";
    String yearParam = "y";
    String yearForBasicMovie = "2019";
    String yearForBasicMovie2 = "2012";
}

