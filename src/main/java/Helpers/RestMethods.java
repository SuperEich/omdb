package Helpers;

import POJO.MovieInfoClass;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;


public class RestMethods implements Config{

    private RequestSpecification requestSpecification;
    private Response response;

    public enum contentType{

        JSON ("application/json"), XML("text/xml");

        private String type;
        contentType(String type){
            this.type = type;
        }
    }

    public void get(String key, String value, int statusCode){
                given().
                    param(key, value).
                when().
                    get(url).
                then().statusCode(statusCode);
    }

    public void createRequestSpecification(String key, String value){
        requestSpecification = given().param(key, value);
    }

    public void addParams(String key, String value) {
        requestSpecification = requestSpecification.param(key, value);

    }

    public Response getResponse(){
        return response = requestSpecification.get(url);
    }

    public void checkResponseCode(int statusCode){
        response.then().statusCode(statusCode);
    }

    public void checkContentType(contentType type){
        Assert.assertTrue(response.contentType().contains(type.type));
    }


    public void checkJSONResponseContains(String path, String value){
        Assert.assertEquals(response.path(path), value);
    }

    public void checkJsonNodeToExist(String path){
        Assert.assertNotNull(response.path(path));
    }

    public void checkXMLResponseContains(String path, String value){
       response.then().assertThat().body(path, equalTo(value));
    }

    public MovieInfoClass jsonToMovieClass(){
        Assert.assertEquals(response.path("Response").toString(), "True");
            return response.as(MovieInfoClass.class);
    }


}
