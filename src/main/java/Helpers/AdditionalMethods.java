package Helpers;

import POJO.MovieInfoClass;
import io.restassured.response.Response;
import org.testng.Assert;

import static org.hamcrest.Matchers.equalTo;

public class AdditionalMethods {

    public void checkObjectEquality(Object o1, Object o2, boolean isEqual){
        Assert.assertEquals(o1.equals(o2), isEqual);
    }

    public void contains(String first, String second){
        Assert.assertTrue(first.toLowerCase().contains(second.toLowerCase()));
    }

    public void equalsMovieXMLtoJSON(MovieInfoClass movie, Response response){

        Assert.assertTrue(response.contentType().contains("text/xml"));

//        movie.getTitle().equals(response.then().body("movie.root.@title"));

        response.then().assertThat().body("root.movie.@title", equalTo(movie.getTitle()));
        response.then().assertThat().body("root.movie.@year", equalTo(movie.getYear()));
        response.then().assertThat().body("root.movie.@released", equalTo(movie.getReleased()));
        response.then().assertThat().body("root.movie.@director", equalTo(movie.getDirector()));
        response.then().assertThat().body("root.movie.@writer", equalTo(movie.getWriter()));
        response.then().assertThat().body("root.movie.@actors", equalTo(movie.getActors()));
        response.then().assertThat().body("root.movie.@plot", equalTo(movie.getPlot()));
        response.then().assertThat().body("root.movie.@language", equalTo(movie.getLanguage()));
        response.then().assertThat().body("root.movie.@country", equalTo(movie.getCountry()));
        response.then().assertThat().body("root.movie.@awards", equalTo(movie.getAwards()));
        response.then().assertThat().body("root.movie.@poster", equalTo(movie.getPoster()));
        response.then().assertThat().body("root.movie.@metascore", equalTo(movie.getMetascore()));
        response.then().assertThat().body("root.movie.@imdbID", equalTo(movie.getImdbID()));
        response.then().assertThat().body("root.movie.@imdbRating", equalTo(movie.getImdbRating()));
        response.then().assertThat().body("root.movie.@imdbVotes", equalTo(movie.getImdbVotes()));
        response.then().assertThat().body("root.movie.@type", equalTo(movie.getType()));
        //TODO There is a problem: no DVD info/boxOfficeInfo/Production/Website/Ratings in XML report. New bug for developers!)
        //response.then().assertThat().body("root.movie.@dvd", equalTo(movie.getDVD()));
        //response.then().assertThat().body("root.movie.@boxOffice", equalTo(movie.getBoxOffice()));
        //response.then().assertThat().body("root.movie.@production", equalTo(movie.getProduction()));
        //response.then().assertThat().body("root.movie.@website", equalTo(movie.getWebsite()));
        response.then().assertThat().body("root.@response", equalTo(movie.getResponse()));


    }
}
