package POJO;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Title",
        "Year",
        "Rated",
        "Released",
        "Runtime",
        "Genre",
        "Director",
        "Writer",
        "Actors",
        "Plot",
        "Language",
        "Country",
        "Awards",
        "Poster",
        "Ratings",
        "Metascore",
        "imdbRating",
        "imdbVotes",
        "imdbID",
        "Type",
        "DVD",
        "BoxOffice",
        "Production",
        "Website",
        "Response"
})
public class MovieInfoClass implements Serializable
{

    @JsonProperty("Title")
    private String title;
    @JsonProperty("Year")
    private String year;
    @JsonProperty("Rated")
    private String rated;
    @JsonProperty("Released")
    private String released;
    @JsonProperty("Runtime")
    private String runtime;
    @JsonProperty("Genre")
    private String genre;
    @JsonProperty("Director")
    private String director;
    @JsonProperty("Writer")
    private String writer;
    @JsonProperty("Actors")
    private String actors;
    @JsonProperty("Plot")
    private String plot;
    @JsonProperty("Language")
    private String language;
    @JsonProperty("Country")
    private String country;
    @JsonProperty("Awards")
    private String awards;
    @JsonProperty("Poster")
    private String poster;
    @JsonProperty("Ratings")
    private List<Rating> ratings = null;
    @JsonProperty("Metascore")
    private String metascore;
    @JsonProperty("imdbRating")
    private String imdbRating;
    @JsonProperty("imdbVotes")
    private String imdbVotes;
    @JsonProperty("imdbID")
    private String imdbID;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("DVD")
    private String dVD;
    @JsonProperty("BoxOffice")
    private String boxOffice;
    @JsonProperty("Production")
    private String production;
    @JsonProperty("Website")
    private String website;
    @JsonProperty("Response")
    private String response;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 3775235800241063817L;


    public MovieInfoClass(String title, String year, String rated, String released, String runtime,
                          String genre, String director, String writer, String actors, String plot,
                          String language, String country, String awards, String poster,
                          List<Rating> ratings, String metascore, String imdbRating, String imdbVotes,
                          String imdbID, String type, String dVD, String boxOffice, String production,
                          String website, String response) {
        setTitle(title);
        setYear(year);
        setRated(rated);
        setReleased(released);
        setRuntime(runtime);
        setGenre(genre);
        setDirector(director);
        setWriter(writer);
        setActors(actors);
        setPlot(plot);
        setLanguage(language);
        setCountry(country);
        setAwards(awards);
        setPoster(poster);
        setRatings(ratings);
        setMetascore(metascore);
        setImdbID(imdbID);
        setImdbRating(imdbRating);
        setImdbVotes(imdbVotes);
        setType(type);
        setDVD(dVD);
        setBoxOffice(boxOffice);
        setProduction(production);
        setWebsite(website);
        setResponse(response);
    }

    @JsonCreator
    public MovieInfoClass() {
    }

    @JsonProperty("Title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("Title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("Year")
    public String getYear() {
        return year;
    }

    @JsonProperty("Year")
    public void setYear(String year) {
        this.year = year;
    }

    @JsonProperty("Rated")
    public String getRated() {
        return rated;
    }

    @JsonProperty("Rated")
    public void setRated(String rated) {
        this.rated = rated;
    }

    @JsonProperty("Released")
    public String getReleased() {
        return released;
    }

    @JsonProperty("Released")
    public void setReleased(String released) {
        this.released = released;
    }

    @JsonProperty("Runtime")
    public String getRuntime() {
        return runtime;
    }

    @JsonProperty("Runtime")
    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    @JsonProperty("Genre")
    public String getGenre() {
        return genre;
    }

    @JsonProperty("Genre")
    public void setGenre(String genre) {
        this.genre = genre;
    }

    @JsonProperty("Director")
    public String getDirector() {
        return director;
    }

    @JsonProperty("Director")
    public void setDirector(String director) {
        this.director = director;
    }

    @JsonProperty("Writer")
    public String getWriter() {
        return writer;
    }

    @JsonProperty("Writer")
    public void setWriter(String writer) {
        this.writer = writer;
    }

    @JsonProperty("Actors")
    public String getActors() {
        return actors;
    }

    @JsonProperty("Actors")
    public void setActors(String actors) {
        this.actors = actors;
    }

    @JsonProperty("Plot")
    public String getPlot() {
        return plot;
    }

    @JsonProperty("Plot")
    public void setPlot(String plot) {
        this.plot = plot;
    }

    @JsonProperty("Language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("Language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("Country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("Country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("Awards")
    public String getAwards() {
        return awards;
    }

    @JsonProperty("Awards")
    public void setAwards(String awards) {
        this.awards = awards;
    }

    @JsonProperty("Poster")
    public String getPoster() {
        return poster;
    }

    @JsonProperty("Poster")
    public void setPoster(String poster) {
        this.poster = poster;
    }

    @JsonProperty("Ratings")
    public List<Rating> getRatings() {
        return ratings;
    }

    @JsonProperty("Ratings")
    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    @JsonProperty("Metascore")
    public String getMetascore() {
        return metascore;
    }

    @JsonProperty("Metascore")
    public void setMetascore(String metascore) {
        this.metascore = metascore;
    }

    @JsonProperty("imdbRating")
    public String getImdbRating() {
        return imdbRating;
    }

    @JsonProperty("imdbRating")
    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    @JsonProperty("imdbVotes")
    public String getImdbVotes() {
        return imdbVotes;
    }

    @JsonProperty("imdbVotes")
    public void setImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    @JsonProperty("imdbID")
    public String getImdbID() {
        return imdbID;
    }

    @JsonProperty("imdbID")
    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("DVD")
    public String getDVD() {
        return dVD;
    }

    @JsonProperty("DVD")
    public void setDVD(String dVD) {
        this.dVD = dVD;
    }

    @JsonProperty("BoxOffice")
    public String getBoxOffice() {
        return boxOffice;
    }

    @JsonProperty("BoxOffice")
    public void setBoxOffice(String boxOffice) {
        this.boxOffice = boxOffice;
    }

    @JsonProperty("Production")
    public String getProduction() {
        return production;
    }

    @JsonProperty("Production")
    public void setProduction(String production) {
        this.production = production;
    }

    @JsonProperty("Website")
    public String getWebsite() {
        return website;
    }

    @JsonProperty("Website")
    public void setWebsite(String website) {
        this.website = website;
    }

    @JsonProperty("Response")
    public String getResponse() {
        return response;
    }

    @JsonProperty("Response")
    public void setResponse(String response) {
        this.response = response;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieInfoClass that = (MovieInfoClass) o;
        return title.equals(that.title)
                && year.equals(that.year)
                && rated.equals(that.rated)
                && released.equals(that.released)
                && runtime.equals(that.runtime)
                && genre.equals(that.genre)
                && director.equals(that.director)
                && writer.equals(that.writer)
                && actors.equals(that.actors)
                && plot.equals(that.plot)
                && language.equals(that.language)
                && country.equals(that.country)
                && awards.equals(that.awards)
                && poster.equals(that.poster)
                && ratings.equals(that.ratings)
                && metascore.equals(that.metascore)
                && imdbRating.equals(that.imdbRating)
                && imdbVotes.equals(that.imdbVotes)
                && imdbID.equals(that.imdbID)
                && type.equals(that.type)
                && dVD.equals(that.dVD)
                && boxOffice.equals(that.boxOffice)
                && production.equals(that.production)
                && website.equals(that.website)
                && response.equals(that.response);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"Title\":\"").append(title).append("\",");
        sb.append("\"Year\":\"").append(year).append("\",");
        sb.append("\"Rated\":\"").append(rated).append("\",");
        sb.append("\"Released\":\"").append(released).append("\",");
        sb.append("\"Runtime\":\"").append(runtime).append("\",");
        sb.append("\"Genre\":\"").append(genre).append("\",");
        sb.append("\"Director\":\"").append(director).append("\",");
        sb.append("\"Writer\":\"").append(writer).append("\",");
        sb.append("\"Actors\":\"").append(actors).append("\",");
        sb.append("\"Plot\":\"").append(plot).append("\",");
        sb.append("\"Language\":\"").append(language).append("\",");
        sb.append("\"Country\":\"").append(country).append("\",");
        sb.append("\"Awards\":\"").append(awards).append("\",");
        sb.append("\"Poster\":\"").append(poster).append("\",");
        sb.append("\"Ratings\":").append(ratings.toString()).append(",");
        sb.append("\"Metascore\":\"").append(metascore).append("\",");
        sb.append("\"imdbRating\":\"").append(imdbRating).append("\",");
        sb.append("\"imdbVotes\":\"").append(imdbVotes).append("\",");
        sb.append("\"imdbID\":\"").append(imdbID).append("\",");
        sb.append("\"Type\":\"").append(type).append("\",");
        sb.append("\"DVD\":\"").append(dVD).append("\",");
        sb.append("\"BoxOffice\":\"").append(boxOffice).append("\",");
        sb.append("\"Production\":\"").append(production).append("\",");
        sb.append("\"Website\":\"").append(website).append("\",");
        sb.append("\"Response\":\"").append(response).append("\"");
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, year, rated, released, runtime, genre, director, writer, actors, plot, language, country, awards, poster, ratings, metascore, imdbRating, imdbVotes, imdbID, type, dVD, boxOffice, production, website, response);
    }
}