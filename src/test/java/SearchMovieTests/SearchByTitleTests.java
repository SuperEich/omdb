package SearchMovieTests;

import Helpers.AdditionalMethods;
import Helpers.RestMethods;
import POJO.MovieInfoClass;
import io.restassured.response.Response;
import org.testng.annotations.Test;

public class SearchByTitleTests extends RestMethods{

    private RestMethods restMethods = new RestMethods();
    private AdditionalMethods additionalMethods = new AdditionalMethods();

    @Test
    public void findAvengersTest(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        restMethods.checkJSONResponseContains("Title", "The Avengers");
    }

    @Test
    public void twoEqualResponsesCheck(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie = restMethods.jsonToMovieClass();

        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie2 = restMethods.jsonToMovieClass();

        additionalMethods.checkObjectEquality(movie, movie2, true);
    }

    @Test
    public void twoDifferentResponsesCheck(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie = restMethods.jsonToMovieClass();

        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, "ave");
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie2 = restMethods.jsonToMovieClass();

        additionalMethods.checkObjectEquality(movie, movie2, false);
    }

    @Test
    public void correctTitleCheck(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie = restMethods.jsonToMovieClass();

        additionalMethods.contains(movie.getTitle(), basicMovie);
    }

    @Test
    public void checkFullPlot(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.addParams(plotParam, plotKeyFull);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        restMethods.checkJsonNodeToExist("Plot");
    }

    @Test
    public void checkYearFilter(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.addParams(yearParam, yearForBasicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        restMethods.checkJSONResponseContains("Year", yearForBasicMovie);
    }

    @Test
    public void checkResultsWithDifferentYears(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.addParams(yearParam, yearForBasicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie = restMethods.jsonToMovieClass();

        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.addParams(yearParam, yearForBasicMovie2);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie2 = restMethods.jsonToMovieClass();

        additionalMethods.checkObjectEquality(movie, movie2, false);
    }

    @Test
    public void checkXMLResponseParam(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.addParams(contentTypeParam, XML);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        restMethods.checkContentType(contentType.XML);
    }

    @Test
    public void checkEqualityOfJsonAndXml(){
        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.addParams(contentTypeParam, XML);
        Response xmlResponse = restMethods.getResponse();

        restMethods.createRequestSpecification(apiParam, apiKey);
        restMethods.addParams(titleParam, basicMovie);
        restMethods.getResponse();
        restMethods.checkResponseCode(200);
        MovieInfoClass movie = restMethods.jsonToMovieClass();

        additionalMethods.equalsMovieXMLtoJSON(movie, xmlResponse);
    }

}
