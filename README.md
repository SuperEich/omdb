# OMDb
To start tests You need to have instaled Maven on your PC.
How to install Maven: https://maven.apache.org/install.html

To run test You have to open console in the OMDb directory and run such task:
mvn test
By this command all the tests will start.

In the future, when we have more test suits we can use such command:
mvn clean test -Dsurefire.suiteXmlFiles=SearchMovieSuite.xml
This command will run exactly suite of SearchMovie tests.

